﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BufferPowerup : MonoBehaviour
{
    public Powerup powerup;
    public int level;
    public int price;
}
